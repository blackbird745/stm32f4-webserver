/*
 * Copyright (c) 2012 Dennis Pfenning
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * 02.01.2012	V0.01	start of development
 *
 * Author: Dennis Pfenning <webmaster@easy-clanpage.de>
 *
 */

#ifndef __FTPD_H__
#define __FTPD_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>

#include "stm32f4xx.h"
#include "lwip/debug.h"
#include "lwip/stats.h"
#include "lwip/tcp.h"
#include "lwip/tcp_impl.h"
#include "software/fatfs/ff.h"
#include "software/fatfs/diskio.h"

#define FTPD_DEBUG				LWIP_DBG_ON
/** How many ftp Connections are allow (1 connection = 1 at port 21 an one at port 20 if data is transfered) */
#define FTPD_MAX_CONNECTIONS	3

/** Port for data transfer (passiv modus): Default is 20 */
#define FTPD_DATA_PORT			20

/** Port for command transfer: Default is 21 */
#define FTPD_SERVER_PORT		21

#define FTPD_TCP_PRIO			TCP_PRIO_MIN

#define FTPD_MAX_USER_LEN		20
#define FTPD_USERNAME			"black"
#define FTPD_PASSWORD			"test"
/** auto disconnect after xx secounds */
#define FTPD_TIMEOUT			60



/** Maximum retries before the connection is aborted/closed.
 * - number of times pcb->poll is called -> default is 4*500ms = 2s;
 * - reset when pcb->sent is called
 */
#ifndef FTPD_MAX_RETRIES
#define FTPD_MAX_RETRIES                   4
#endif

/** The poll delay is X*500ms */
#ifndef FTPD_POLL_INTERVAL
#define FTPD_POLL_INTERVAL                 1000 / TCP_SLOW_INTERVAL
#endif


enum ftpd_state_e {
    FTPD_USER,
    FTPD_PASS,
    FTPD_IDLE,
    FTPD_NLST,
	FTPD_LIST,
	FTPD_RETR,
	FTPD_RNFR,
	FTPD_STOR,
	FTPD_QUIT
};

struct ftpd_datastate
{
    int connected;
	FIL *fil;
	DIR *dir;
	FILINFO *fno;
	char* buf;							/* Sendbuffer */
    int buf_size;
    u16_t unsend_bytes;
	int filepos;						/* current fileposition */
	int retries;
    struct tcp_pcb *msgpcb;
    struct ftpd_msgstate *msgfs;
};

struct ftpd_msgstate
{
    enum ftpd_state_e state;
    char* buf;
	u16_t dataport;
	int passive;
	char *renamefrom;
	int retries;
	char username[FTPD_MAX_USER_LEN + 1];
	char path[255];
	int timeout;
    struct ip_addr dataip;
    struct tcp_pcb *datapcb;
    struct tcp_pcb *listenpcb;
    struct ftpd_datastate *datafs;
};

struct ftpd_command
{
    char *cmd;
    void (*func) (const char *arg, struct tcp_pcb * pcb, struct ftpd_msgstate * fsm);
};

void ftpd_init(void);

#define msg110 "110 MARK %s = %s."
#define msg120 "120 Service ready in nnn minutes."
#define msg125 "125 Data connection already open; transfer starting."
#define msg150 "150 File status okay; about to open data connection."
#define msg150recv "150 Opening BINARY mode data connection for %s (%i bytes)."
#define msg150stor "150 Opening BINARY mode data connection for %s."
#define msg200 "200 Command okay."
#define msg202 "202 Command not implemented, superfluous at this site."
#define msg211 "211 System status, or system help reply."
#define msg212 "212 Directory status."
#define msg213 "213 %i"
#define msg214 "214 %s."
#define msg214SYST "214 %s system type."
#define msg220 "220 Enlogic PDU FTP Server ready."
#define msg221 "221 Goodbye."
#define msg225 "225 Data connection open; no transfer in progress."
#define msg226 "226 Closing data connection."
#define msg227 "227 Entering Passive Mode (%i,%i,%i,%i,%i,%i)."
#define msg230 "230 User logged in, proceed."
#define msg250 "250 Requested file action okay, completed."
#define msg257PWD "257 \"%s\" is current directory."
#define msg257 "257 \"%s\" created."
#define msg331 "331 Please specify the password."
#define msg332 "332 Need account for login."
#define msg350 "350 Requested file action pending further information."
#define msg421 "421 Service not available, closing control connection."
#define msg421_user "421 Too many users connected."
#define msg421_timeout "421 No transfer timeout (%i seconds): closing control connection"
#define msg425 "425 Can't open data connection."
#define msg426 "426 Connection closed; transfer aborted."
#define msg450 "450 Requested file action not taken."
#define msg451 "451 Requested action aborted: local error in processing."
#define msg452 "452 Requested action not taken."
#define msg500 "500 Syntax error, command unrecognized."
#define msg501 "501 Syntax error in parameters or arguments."
#define msg502 "502 Command not implemented."
#define msg503_user "503 Login with USER first."
#define msg504 "504 Command not implemented for that parameter."
#define msg530 "530 Login incorrect."
#define msg530_login_first "530 Login first with USER and PASS."
#define msg532 "532 Need account for storing files."
#define msg550 "550 Requested action not taken."
#define msg550_rnfr "550 send RNFR first."
#define msg551 "551 Requested action aborted: page type unknown."
#define msg552 "552 Requested file action aborted."
#define msg553 "553 Requested action not taken."


#endif /* __FTPD_H__ */
