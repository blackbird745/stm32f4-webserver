/*
 * fsdata_custom.c
 *
 *  Created on: Dec 30, 2011
 *      Author: Black Bird
 */

int fs_open_custom(struct fs_file *file, const char *name)
{
	if (f_open(&file->fsfile, name, FA_OPEN_EXISTING | FA_READ) == 0) {
		f_stat(name, &Finfo);
		file->index = 0;
		file->len = Finfo.fsize;
		file->http_header_included = 0;
		return 1;
	} else {
		return 0;
	}
}
void fs_close_custom(struct fs_file *file)
{
	f_close(&file->fsfile);
}
