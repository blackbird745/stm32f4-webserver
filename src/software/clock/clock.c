/*
 * Copyright (c) 2007,2008 by Christian Dietrich <stettberger@dokucode.de>
 * Copyright (c) 2009 by Dirk Pannenbecker <dp@sd-gp.de>
 * Copyright (c) 2009 by Stefan Siegl <stesie@brokenpipe.de>
 * Copyright (c) 2011-2012 by Erik Kunze <ethersex@erik-kunze.de>
 * (c) by Alexander Neumann <alexander@bumpern.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 */

#include "main.h"
#include "software/ntp/ntp.h"
#include "clock.h"

static timestamp_t clock_timestamp;
static timestamp_t sync_timestamp;
static timestamp_t n_sync_timestamp;
static timestamp_t n_sync_tick;
static int16_t delta;
static uint16_t ntp_count;


uint16_t ntp_timer = 4, ntp_retry=0;
timestamp_t uptime_timestamp;

extern uint32_t LsiFreq;

void
clock_init(void)
{

}

void
clock_tick(void)
{
	if (!sync_timestamp || sync_timestamp == clock_timestamp)
	{
		clock_timestamp++;
		uptime_timestamp++;
	}
	if (sync_timestamp)
		sync_timestamp++;


	if (ntp_timer)
		ntp_timer--;
	else {
		if(ntp_retry) {
			ntp_retry--;
		} else {
			if(!ntp_retry) sntp_retry(NULL);
			sntp_request(NULL);
		}
	}
}

void
clock_set_time_raw(timestamp_t new_sync_timestamp)
{
  clock_timestamp = new_sync_timestamp;
}

void
clock_set_time(timestamp_t new_sync_timestamp)
{
  /* The clock was synced */
  if (sync_timestamp)
  {
    delta = new_sync_timestamp - sync_timestamp;
    NTPADJDEBUG("sync timestamp delta is %d\n", delta);
    if (delta < -300 || delta > 300)
      NTPADJDEBUG("eeek, delta too large. " "not adjusting.\n");

    else if (sync_timestamp != clock_timestamp)
      NTPADJDEBUG("our clock is not up with ntp clock.\n");

    else if (NTP_RESYNC_PERIOD == -delta)
      NTPADJDEBUG("-delta equals resync period, eeek!? "
                  "clock isn't running at all.\n");
    else
    {
      LsiFreq *= NTP_RESYNC_PERIOD;
      LsiFreq /= NTP_RESYNC_PERIOD + delta;
      NTPADJDEBUG("new OCR1A value %lu\n", LsiFreq);
      RTC_InitStructure.RTC_SynchPrediv	=  (LsiFreq/16) - 1;
      RTC_Init(&RTC_InitStructure);
    }

  }
  sync_timestamp = new_sync_timestamp;
  n_sync_timestamp = new_sync_timestamp;

  clock_reset_dst_change();

  /* Allow the clock to jump forward, but not to go backward
   * except the time difference is greater than 5 minutes */
  if (sync_timestamp > clock_timestamp ||
      (clock_timestamp - sync_timestamp) > 300)
    clock_timestamp = sync_timestamp;

  ntp_timer = NTP_RESYNC_PERIOD;
}

timestamp_t
clock_get_time(void)
{
  return clock_timestamp;
}

timestamp_t
clock_last_sync(void)
{
  return n_sync_timestamp;
}

timestamp_t
clock_last_sync_tick(void)
{
  return n_sync_tick;
}

int16_t
clock_last_delta(void)
{
  return delta;
}

uint16_t
clock_ntp_count(void)
{
  return ntp_count;
}

void
set_ntp_count(const uint16_t new_ntp_count)
{
  ntp_count = (new_ntp_count == 0) ? 0 : ntp_count + new_ntp_count;
}

uint16_t
clock_last_ntp(void)
{
  return ntp_timer;
}

timestamp_t
clock_get_uptime(void)
{
  return uptime_timestamp;
}
