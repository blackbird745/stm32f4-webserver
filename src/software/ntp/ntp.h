/*
 * ntp.h
 *
 *  Created on: Jan 29, 2012
 *      Author: black
 */

#ifndef NTP_H_
#define NTP_H_

#include "main.h"
#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/tcp.h"
#include "hardware/rtc/rtc.h"
#include "software/clock/clock.h"
#include <string.h>
#include <stdio.h>

extern uint16_t ntp_retry, ntp_timer;
extern RTC_InitTypeDef RTC_InitStructure;

#define NTP_RESYNC_PERIOD 600

struct ntp_date_time {
  uint32_t seconds;
  uint32_t fraction;
};

struct ntp_packet {
  uint8_t  li_vn_mode;                    /* leap indicator, version and mode */
  uint8_t  stratum;                       /* peer stratum */
  uint8_t  ppoll;                         /* peer poll interval */
  signed char precision;                  /* peer clock precision */
  uint32_t rootdelay;                     /* distance to primary clock */
  uint32_t rootdispersion;                /* clock dispersion */
  uint32_t refid;                         /* reference clock ID */
  struct ntp_date_time    reftime;        /* time peer clock was last updated */
  struct ntp_date_time    org;            /* originate time stamp */
  struct ntp_date_time    rec;            /* receive time stamp */
  struct ntp_date_time    xmt;            /* transmit time stamp */
};

struct clock_datetime_t {
    uint8_t sec;
    union {
        uint8_t cron_fields[5];
        struct {
            uint8_t min;
            uint8_t hour;
            uint8_t day;
            uint8_t month;
            uint8_t dow;
        };
    };
    uint8_t year;
};

void sntp_init(void);
void ntp_update_time(uint32_t);
void ntp_sync(void);
void ntp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, struct ip_addr *addr, u16_t port);
void sntp_request(void *arg);
void sntp_retry(void *arg);


#endif /* NTP_H_ */
