/**
*****************************************************************************
**
**  File        : templog.c
**
**  Abstract    : main function.
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed �as is,� without any warranty
**                of any kind.
**
**  (c)Copyright Atollic AB.
**  You may use this file as-is or modify it according to the needs of your
**  project. Distribution of this file (unmodified or modified) is not
**  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
**  rights to distribute the assembled, compiled & linked contents of this
**  file as part of an application binary file, provided that it is built
**  using the Atollic TrueSTUDIO(R) toolchain.
**
**
*****************************************************************************
*/

/* Includes */
#include <stdlib.h>
#include "main.h"
#include "templog.h"
#include "crc8.h"
#include "hardware/sht11/sht11.h"


/* Private typedef */
/* Private define */
/* Private macro */
/* Private variables */
struct sensor_t 		measdata[MAX_SENSORS];
extern uint32_t 		rfm12_crc_erros;
extern 	FIL file;
/* Private function prototypes */
/* Private functions */

/**
  * @brief  init the templog data array
  * @param  None
  * @retval None
  */
void templog_init()
{
	uint8_t j,i;
	for(i = 0;i<MAX_SENSORS; i++) {
		measdata[i].aktiv = 0;
		measdata[i].last_value_index = MAX_VALUES_PER_SENSOR;
		measdata[i].sensortyp = 0;
	}
	measdata[0].values = (struct value_sht11*)malloc(sizeof(struct value_sht11) * MAX_VALUES_PER_SENSOR);
	memset(measdata[0].values, 0, sizeof(struct value_sht11) * MAX_VALUES_PER_SENSOR);
        measdata[0].last_value_index = 0;
}
/**
  * @brief  init the templog data array
  * @param  None
  * @retval None
  */
void templog_perodic() {
	if (rfm12_rx_status() == STATUS_COMPLETE)  {
		if(rfm12_rx_len() > RFM12_RX_BUFFER_SIZE)
		{
			rfm12_rx_clear();
			return;
		}
		uint8_t *bufcontents, rx_len;
		bufcontents = rfm12_rx_buffer();
		/* dump buffer contents to uart
		for (uint8_t i=0;i<rfm12_rx_len();i++)
		{
			uart_putc ( bufcontents[i] );
		}*/
		rx_len = rfm12_rx_len();
		if(rx_len > 3) {
			if(crc8(bufcontents, rx_len-1) != bufcontents[rx_len-1]) {
					rfm12_crc_erros++;
					/*uart_puts_P("crc: ");
					uart_putc(crc8(bufcontents, rx_len-1));
					uart_putc(' ');
					uart_putc(bufcontents[rx_len-1]);
					uart_putc('\n');*/
					#ifdef RFM12_ANSWER_TO_SENSORS
					answer[0] = bufcontents[1];
					answer[1] = SENSOR_ID;
					answer[2] = RFM12_NACK;
					answer[3] = crc8(answer, 3);
					msg_length = 4;
					while(rfm12_tx (msg_length, 0x23, answer) != 0x80)
						rfm12_tick();
	   				#endif
			} else {
				if(bufcontents[0] == SENSOR_ID) {
					if (bufcontents[2] == RFM12_REG_S) {
						#ifdef RFM12_ANSWER_TO_SENSORS
						answer[0] = bufcontents[1];
						answer[1] = SENSOR_ID;
						answer[2] = RFM12_REG_R;
						answer[3] = sensor_timer;
						answer[4] = crc8(answer, 4);
						msg_length = 5;
						while(rfm12_tx (msg_length, 0x23, answer) != 0x80)
							rfm12_tick();
						#endif
						/* TODO */
						// SENSORID im RAM und EEPROM eintragen
						if(measdata[bufcontents[1]-1].sensortyp == 0) {
							if(bufcontents[3] == SENSOR_DS1820) {
								measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
								measdata[bufcontents[1]-1].values = (struct value_ds18x20*)malloc(sizeof(struct value_ds18x20) * MAX_VALUES_PER_SENSOR);
							} else if (bufcontents[3] == SENSOR_SHT11) {
								measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
								measdata[bufcontents[1]-1].values = (struct value_sht11*)malloc(sizeof(struct value_sht11) * MAX_VALUES_PER_SENSOR);
							} else if (bufcontents[3] == SENSOR_HP03S) {
								measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
								measdata[bufcontents[1]-1].values = (struct value_hp03*)malloc(sizeof(struct value_hp03) * MAX_VALUES_PER_SENSOR);
							}
						}
					} else if ( bufcontents[2] == RFM12_MEAS) {
						/* TODO */
						#ifdef RFM12_ANSWER_TO_SENSORS
						answer[0] = bufcontents[1];
						answer[1] = SENSOR_ID;
						answer[2] = RFM12_ACK;
						answer[3] = sensor_timer;
						answer[4] = crc8(answer, 4);
						msg_length = 5;
						while(rfm12_tx (msg_length, 0x23, answer) != 0x80)
							rfm12_tick();
						#endif
						if(clock_get_time() > 1000000) {
							if(measdata[bufcontents[1]-1].sensortyp == 0) {
								if(bufcontents[3] == SENSOR_DS1820) {
									measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
									measdata[bufcontents[1]-1].values = (struct value_ds18x20*)malloc(sizeof(struct value_ds18x20) * MAX_VALUES_PER_SENSOR);
									memset(measdata[bufcontents[1]-1].values, 0, sizeof(struct value_ds18x20) * MAX_VALUES_PER_SENSOR);
								} else if (bufcontents[3] == SENSOR_SHT11) {
									measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
									measdata[bufcontents[1]-1].values = (struct value_sht11*)malloc(sizeof(struct value_sht11) * MAX_VALUES_PER_SENSOR);
									memset(measdata[bufcontents[1]-1].values, 0, sizeof(struct value_sht11) * MAX_VALUES_PER_SENSOR);
								} else if (bufcontents[3] == SENSOR_HP03S) {
									measdata[bufcontents[1]-1].sensortyp = bufcontents[3];
									measdata[bufcontents[1]-1].values = (struct value_hp03*)malloc(sizeof(struct value_hp03) * MAX_VALUES_PER_SENSOR);
									memset(measdata[bufcontents[1]-1].values, 0, sizeof(struct value_hp03) * MAX_VALUES_PER_SENSOR);
								}
							}
							if(++measdata[bufcontents[1]-1].last_value_index > MAX_VALUES_PER_SENSOR) {
								measdata[bufcontents[1]-1].last_value_index = 0;
							}
							measdata[bufcontents[1]-1].aktiv = 1;
							if(measdata[bufcontents[1]-1].sensortyp == SENSOR_DS1820)
							{
								struct value_ds18x20 *data;
								data = (struct value_ds18x20 *)measdata[bufcontents[1]-1].values;
								data[measdata[bufcontents[1]-1].last_value_index].battery = ((((uint16_t)bufcontents[4])<<8) | bufcontents[5]);
								data[measdata[bufcontents[1]-1].last_value_index].timestamp = clock_get_time();
								data[measdata[bufcontents[1]-1].last_value_index].temp = ((((int16_t)bufcontents[7])<<8) | bufcontents[6]);
							} else if (measdata[bufcontents[1]-1].sensortyp == SENSOR_SHT11) {
								struct value_sht11 *data;
								data = (struct value_sht11 *)measdata[bufcontents[1]-1].values;
								data[measdata[bufcontents[1]-1].last_value_index].battery = ((((uint16_t)bufcontents[4])<<8) | bufcontents[5]);
								data[measdata[bufcontents[1]-1].last_value_index].timestamp = clock_get_time();
								data[measdata[bufcontents[1]-1].last_value_index].temp = ((((int16_t)bufcontents[6])<<8) | bufcontents[7]);
								data[measdata[bufcontents[1]-1].last_value_index].humi = ((((int16_t)bufcontents[8])<<8) | bufcontents[9]);
							} else if (measdata[bufcontents[1]-1].sensortyp == SENSOR_HP03S) {
								struct value_hp03 *data;
								data = (struct value_hp03 *)measdata[bufcontents[1]-1].values;
								data[measdata[bufcontents[1]-1].last_value_index].battery = ((((uint16_t)bufcontents[4])<<8) | bufcontents[5]);
								data[measdata[bufcontents[1]-1].last_value_index].timestamp = clock_get_time();
								data[measdata[bufcontents[1]-1].last_value_index].temp = ((((int16_t)bufcontents[6])<<8) | bufcontents[7]);
								data[measdata[bufcontents[1]-1].last_value_index].pressure = ((((int16_t)bufcontents[8])<<8) | bufcontents[9]);
							}
						}
					#ifdef RFM12_ANSWER_TO_SENSORS
					} else if ( bufcontents[2] == RFM12_NACK) {
						while(rfm12_tx (msg_length, 0x23, answer) != 0x80)
							rfm12_tick();
					#endif
					} else if ( bufcontents[2] == RFM12_ACK ) {

					} else {

					}
				}
			}
		}
		rfm12_rx_clear();
	}
}
void templog_write_data()
{
	static uint32_t lastlog = 0;
	UINT bytes_write = 0;
	char buf[1000];

	clock_datetime_t date;
	clock_localtime(&date, clock_get_time()-1);

	//TODO Fehler bei Ordner anlegen oder wechsel
	sprintf((char *)buf, "/%u", 1900+date.year);
	if(f_chdir((char *)buf))
	{
		f_mkdir((char *)buf);
		f_chdir((char *)buf);

	} // %u/    1900+date.year,

	sprintf((char *)buf,"%02i%02i%02i.csv",  date.year%100 ,date.month, date.day);
	printf("Templog: File: %s\n", buf);
	if(!f_open(&file, (char *)buf, FA_OPEN_ALWAYS | FA_WRITE)) {
		buf[0] = 0;
		f_lseek(&file, file.fsize);
		printf("Templog: File: geoeffnet\n");
		uint8_t i;
		for(i = 0; i<MAX_SENSORS; i++) {
			if(measdata[i].sensortyp == SENSOR_DS1820)
			{
				struct value_ds18x20 *data = (struct value_ds18x20 *)measdata[i].values;
				if(lastlog < data[measdata[i].last_value_index].timestamp && measdata[i].aktiv == 1) {
					clock_localtime(&date, data[measdata[i].last_value_index].timestamp);
					sprintf(buf, "%s%2.2i:%2.2i:%2.2i;%i;%i;%li.%i;;%u.%03u\n",
						buf, date.hour, date.min, date.sec, i+1, measdata[i].sensortyp,
						(data[measdata[i].last_value_index].temp * 625L) / 10000,
						abs((data[measdata[i].last_value_index].temp * 625L) % 10000),
						data[measdata[i].last_value_index].battery / 1000, data[measdata[i].last_value_index].battery % 1000);
				}
			} else if (measdata[i].sensortyp == SENSOR_SHT11) {
				struct value_sht11 *data = (struct value_sht11 *)measdata[i].values;
				if(lastlog < data[measdata[i].last_value_index].timestamp && measdata[i].aktiv == 1) {
					clock_localtime(&date, data[measdata[i].last_value_index].timestamp);
					sprintf(buf, "%s%2.2i:%2.2i:%2.2i;%i;%i;%i.%i;%u.%u;%u.%03u\n",
						buf, date.hour, date.min, date.sec, i+1, measdata[i].sensortyp,
						data[measdata[i].last_value_index].temp / 100,
						abs(data[measdata[i].last_value_index].temp % 100),
						data[measdata[i].last_value_index].humi / 100,
						data[measdata[i].last_value_index].humi % 100,
						data[measdata[i].last_value_index].battery / 1000, data[measdata[i].last_value_index].battery % 1000);
				}
			} else if (measdata[i].sensortyp == SENSOR_HP03S) {
				struct value_hp03 *data = (struct value_hp03 *)measdata[i].values;
				if(lastlog < data[measdata[i].last_value_index].timestamp && measdata[i].aktiv == 1) {
					clock_localtime(&date, data[measdata[i].last_value_index].timestamp);
					sprintf(buf, "%s%2.2i:%2.2i:%2.2i;%i;%i;%i.%i;%i.%i;%u.%03u\n",
						buf, date.hour, date.min, date.sec, i+1, measdata[i].sensortyp,
						data[measdata[i].last_value_index].temp / 100,
						abs(data[measdata[i].last_value_index].temp % 100),
						data[measdata[i].last_value_index].pressure / 10,
						data[measdata[i].last_value_index].pressure % 10,
						data[measdata[i].last_value_index].battery / 1000, data[measdata[i].last_value_index].battery % 1000);
				}
			} else {
				continue;
			}
		}
		if(strlen((char *)buf)) {
			f_write(&file, buf, strlen((char *)buf), &bytes_write);
			f_sync(&file);
		}
		f_close(&file);
	}
	f_chdir("/");
	lastlog = clock_get_time();
}
void temp_local_meas() {
	static uint8_t new_meas;
	if(SENSOR_TYP == SENSOR_SHT11) {
                struct value_sht11 *data = (struct value_sht11 *)measdata[0].values;
		uint32_t zeit = data[measdata[0].last_value_index].timestamp + 20;
		if(new_meas == 0 && clock_get_time() >  zeit) {
			sht11_start_measure();
			new_meas = 1;
			printf("SHT11: MEAS start\n");
		}
		if(new_meas == 1 && sht11_measure_finish()) {
			if(++measdata[0].last_value_index > MAX_VALUES_PER_SENSOR) {
				measdata[0].last_value_index = 0;
			}
                        data[measdata[0].last_value_index].battery = 3300; //ADC_Get_Vcc();
			measdata[0].aktiv = 1;
			data[measdata[0].last_value_index].timestamp = clock_get_time();
			measdata[0].sensortyp = SENSOR_SHT11;
			data[measdata[0].last_value_index].temp = sht11_get_tmp();
			data[measdata[0].last_value_index].humi = (uint16_t)sht11_get_hum();
			printf("SHT11: T: %i.%02i H: %u.%02u\n", data[measdata[0].last_value_index].temp/100, data[measdata[0].last_value_index].temp%100, data[measdata[0].last_value_index].humi/100, data[measdata[0].last_value_index].humi%100);
			new_meas = 0;
		}
	}
}
