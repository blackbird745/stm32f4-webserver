/**
 *****************************************************************************
 **
 **  File        : main.c
 **
 **  Abstract    : main function.
 **
 **  Functions   : main
 **
 **  Environment : Atollic TrueSTUDIO(R)
 **                STMicroelectronics STM32F4xx Standard Peripherals Library
 **
 **  Distribution: The file is distributed �as is,� without any warranty
 **                of any kind.
 **
 **  (c)Copyright Atollic AB.
 **  You may use this file as-is or modify it according to the needs of your
 **  project. Distribution of this file (unmodified or modified) is not
 **  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
 **  rights to distribute the assembled, compiled & linked contents of this
 **  file as part of an application binary file, provided that it is built
 **  using the Atollic TrueSTUDIO(R) toolchain.
 **
 **
 *****************************************************************************
 */

/* Includes */
#include "main.h"
#include "netconf.h"
#include "periodic.h"
#include "hardware/rfm12/rfm12.h"
#include "software/cron/cron_static.h"
#include "hardware/sht11/sht11.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro */
/* Private variables */
__IO uint8_t new_sec = 0, new_tick;
__IO uint32_t LocalTime = 0; /* this variable is used to create a time reference incremented by 10ms */
/* Private function prototypes */
/* Private functions */

void periodic_main(void)
{
	if(new_tick)
	{
		/* handle periodic timers for LwIP */
		LwIP_Periodic_Handle(LocalTime);
		new_tick = 0;
	}
	if(LocalTime%500 == 0) {
		sht11_measure_finish();
	}
	if(new_sec) {
		clock_tick();
		cron_static_periodic();
		temp_local_meas();

		new_sec = 0;
		uint8_t rfm_status;
		rfm_status = rfm12_read(0x0000);
		if(GPIO_ReadInputDataBit(RFM12_nIRQ_GPIO_PORT,RFM12_nIRQ_PIN) == Bit_RESET || rfm_status == 0) {
			printf("RFM12: %X\n", rfm_status);
			printf("RFM12 reinit\n");
			rfm12_init();
			rfm_status = rfm12_read(0x0000);
			printf("RFM12: %X\n", rfm_status);
		}
	}
}
/**
 * @brief  Updates the system local time
 * @param  None
 * @retval None
 */
void Time_Update(void)
{
	LocalTime += SYSTEMTICK_PERIOD_MS;
}
