/**
 *****************************************************************************
 **
 **  File        : main.c
 **
 **  Abstract    : main function.
 **
 **  Functions   : main
 **
 **  Environment : Atollic TrueSTUDIO(R)
 **                STMicroelectronics STM32F4xx Standard Peripherals Library
 **
 **  Distribution: The file is distributed �as is,� without any warranty
 **                of any kind.
 **
 **  (c)Copyright Atollic AB.
 **  You may use this file as-is or modify it according to the needs of your
 **  project. Distribution of this file (unmodified or modified) is not
 **  permitted. Atollic AB permit registered Atollic TrueSTUDIO(R) users the
 **  rights to distribute the assembled, compiled & linked contents of this
 **  file as part of an application binary file, provided that it is built
 **  using the Atollic TrueSTUDIO(R) toolchain.
 **
 **
 *****************************************************************************
 */

/* Includes */
#include "main.h"
#include "netconf.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro */
/* Private variables */
uint32_t rfm12_crc_erros, rfm12_pack_recv, rfm12_pack_send;
uint32_t net_bytes_send, net_bytes_recv;
FILINFO Finfo;
FIL file;
char Lfname[_MAX_LFN+1];
FATFS Fatfs[_VOLUMES];		/* File system object for each logical drive */
/* Private function prototypes */
/* Private functions */
uint16_t test;
/**
 **===========================================================================
 **
 **  Abstract: main program
 **
 **===========================================================================
 */
int main(void)
{
	uint8_t buffer[32];
	uint8_t len;

	RCC_ClocksTypeDef RCC_Clocks;

	usart_init(UART_BAUDRATE);
	printf("UART init done...\n");

	rtc_init();

	/* Configure Systick clock source as HCLK */
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

	/* SystTick configuration: an interrupt every 10ms */
	RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

	Finfo.lfname = Lfname;
	Finfo.lfsize = sizeof(Lfname);

	printf("DISK: %d\n", disk_initialize(0));
	printf("SD: %d\n", f_mount(0, &Fatfs[0]));

	/* configure ethernet (GPIOs, clocks, MAC, DMA) */
	ETH_BSP_Config();
	/* Initilaize the LwIP stack */
	LwIP_Init();
	/* Http webserver Init */
	httpd_init();
	/* ftpd webserver Init */
	ftpd_init();

	sntp_init();

	rfm12_init();

	// Initialize AVR for use with mirf
	mirf_init();
	// Wait for mirf to come up
	//while(LocalTime%100 > 0);
	//mirf_set_RADDR((uint8_t *)"clie1");
	// Configure mirf
	mirf_config();
	// Test communication
	uint8_t rf_setup = 0;
	mirf_read_register( STATUS, &rf_setup, sizeof(rf_setup) );
	printf( "rf_setup = %x\n", rf_setup );

	sht11_init();

	templog_init();

	while (1)
	{
		rfm12_tick();
		polling();
		templog_perodic();
		periodic_main();
		/* check if any packet received */
		if (ETH_CheckFrameReceived())
		{
			/* process received ethernet packet */
			LwIP_Pkt_Handle();
		}

//			if(mirf_data_ready()) {
//				mirf_get_data(buffer, &len);
//				if(len >0 && len<=32) {
//					uint8_t i;
//					for(i = 0; i<len;i++)
//					{
//						putch(buffer[i]);
//					}
//					putch('\n');
//				}
//			}
	}
}
